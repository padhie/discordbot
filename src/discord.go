package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"os"
	"os/signal"
	"syscall"
)

var BotId string
var BotName string

func loginDiscord() {
	dg, err := discordgo.New("Bot " + _config.Discord.AuthToken)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	u, err := dg.User("@me")
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	BotId = u.ID
	BotName = u.Username

	dg.AddHandler(messageHandler)
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	//dg.Close()
}

func messageHandler(session *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == BotId {
		return
	}

	fmt.Println(fmt.Sprintf("<(%s) %s: %s", m.ChannelID, m.Author.Username, m.Content))

	for i := 0; i < len(_config.Commands); i++ {
		if m.Content == _config.Commands[i].Command {
			writeDiscord(session, m.ChannelID, _config.Commands[i].Text)
		}
	}
}

func writeDiscord(session *discordgo.Session, channelId string, message string) {
	fmt.Println(fmt.Sprintf(">(%s) %s: %s", channelId, BotName, message))

	session.ChannelMessageSend(channelId, message)
}
