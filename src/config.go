package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

var configFile = "./config.json"
var _config Config

type Config struct {
	Discord  DiscordConfig `json:"discord"`
	Commands []Command     `json:"commands"`
}

type DiscordConfig struct {
	AuthToken string `json:"authToken"`
}

type Command struct {
	Command string `json:"command"`
	Text    string `json:"text"`
}

func readConfig() {
	absPath, _ := filepath.Abs(configFile)
	jsonFile, err := os.Open(absPath)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config Config
	err = json.Unmarshal(byteValue, &config)
	if err != nil {
		log.Println(err)
		return
	}

	_config = config
}
